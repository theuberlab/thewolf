package pkg

import (
	"os"
	"time"
	"math/big"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"

	"bitbucket.org/theuberlab/lug"
)

// Generate a private/public key pair for the specified key size.
func GenKeyPair(keySize int) (*rsa.PrivateKey, *rsa.PublicKey){
	reader := rand.Reader

	privateKey, err := rsa.GenerateKey(reader, keySize)
	if err != nil {
		lug.Error("Message", "Failed to generate key", "Error", err)
		os.Exit(1)
	}

	publicKey := privateKey.PublicKey

	return privateKey, &publicKey
}

// Generates a certificate for the provided key pair. Is currently returned as a []byte containing the certificate in PEM format.
func GenCert(privKey *rsa.PrivateKey, pubKey *rsa.PublicKey) []byte {
	notBefore := time.Now()

	notAfter := notBefore.Add(365 * 24 * time.Hour)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		lug.Error("Message", "failed to generate serial number", "Error", err)
		os.Exit(1)
	}

	// Create a template for the certificate. The subject is minimal because it will be ignored by browsers due to the presense of the SAN.
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"The Uberlab"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	// Add DNSNames go generate the SAN entries.
	template.DNSNames = append(template.DNSNames, "localhost")
	template.DNSNames = append(template.DNSNames, "localhost.localdomain")

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pubKey, privKey)
	if err != nil {
		lug.Error("Message", "Failed to create certificate", "Error", err)
		os.Exit(1)
	}

	pemBytes := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})

	return pemBytes
}

// Generates a certificate for the provided key pair. Is currently returned as a []byte containing the certificate in PEM format.
func GenCertExt(privKey *rsa.PrivateKey, pubKey *rsa.PublicKey, emails []string) []byte {
	notBefore := time.Now()

	notAfter := notBefore.Add(365 * 24 * time.Hour)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		lug.Error("Message", "failed to generate serial number", "Error", err)
		os.Exit(1)
	}

	//TODO: Take a ref to an x509.Certificate and generate one in a function which can do some validation.

	// Create a template for the certificate. The subject is minimal because it will be ignored by browsers due to the presense of the SAN.
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization:		[]string{"ORGANIZATION_NAME"},
			Country:			[]string{"COUNTRY_CODE"},
			Province:			[]string{"PROVINCE"},
			Locality:			[]string{"CITY"},
			StreetAddress:		[]string{"ADDRESS"},
			PostalCode: 		[]string{"POSTAL_CODE"},
			CommonName: 		"COMMON_NAME",

		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	// Add DNSNames go generate the SAN entries.
	template.DNSNames = append(template.DNSNames, "localhost")
	template.DNSNames = append(template.DNSNames, "localhost.localdomain")
	template.EmailAddresses = append(template.EmailAddresses, emails...)


	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, pubKey, privKey)
	if err != nil {
		lug.Error("Message", "Failed to create certificate", "Error", err)
		os.Exit(1)
	}

	pemBytes := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})

	return pemBytes
}

//TODO: Add some validation of these fields which is the main reason to have this as a function.



//TODO: Figure out what I want to do here. Have a bit like this that generats just the subject and then validate the other fields up in the cert generation?
// Or be more intelligent and make the below generate the entire cert template? Prolly that one.
// If we do the latter we will determine if it's going to be a cert signing cert or not in this function.

//TODO: Turn this whole thing into a cert library so that I can stop copying and pasting gencertkey.go and so that I
// can produce a tiny little stand alone openssl like tool for generating and signing certs.

func GenSubject(organization string, country string, province string, locality string, street string, code string, cn string) (pkix.Name, error) {
	return pkix.Name{
		Organization:		[]string{organization},
		Country:			[]string{country},
		Province:			[]string{province},
		Locality:			[]string{locality},
		StreetAddress:		[]string{street},
		PostalCode: 		[]string{code},
		CommonName: 		cn,
	}, nil
}