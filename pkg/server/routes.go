package server

import (
	"bitbucket.org/theuberlab/lug"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// I don't generally agree with having this in a completely separate file like this but
// routing will eventually be determined by the implemented plugins so breaking it up in this way
// is a logical first step.
func (s *Server) MakeRoutes() {
	lug.Debug("Message", "Adding routes to mux")

	pluginsArray := s.PluginManager.GetAuthPluginOrder()

	//TODO: Add a base URL and an about for humans
	//s.Router.HandleFunc("/api/", s.handleAPI())
	//s.Router.HandleFunc("/about", s.handleAbout())



	// swagger:route GET / swagger
	//
	// Returns the actual swagger specification
	//
	//     Consumes:
	//     - text/html
	//
	//     Produces:
	//     - application/json
	//
	//     Responses:
	//       200: someResponse
	s.Router.HandleFunc("/swagger.json", s.swaggerSpec()).Methods("GET")

	// swagger:route GET / usage
	//
	// Returns some directions for humans
	//
	//     Consumes:
	//     - text/html
	//
	//     Produces:
	//     - text/html
	//
	//     Responses:
	//       200: someResponse
	s.Router.HandleFunc("/", s.handleRoot()).Methods("GET")

	// Not sure how to enable individual metrics when I'm doing them the way I'm doing them.

	// swagger:route GET /metrics prometheusmetrics
	//
	// Returns application metrics in prometheus format
	//
	// This will show all available pets by default.
	// You can get the pets that are out of stock
	//
	//     Consumes:
	//     - text/html
	//     - application/x-protobuf
	//
	//     Produces:
	//     - text/html
	//     - application/x-protobuf
	//
	//     Responses:
	//       200: someResponse
	s.Router.Handle("/metrics", promhttp.Handler())

	// swagger:route POST /webhook HandleWithAuthorization
	//
	// Accept webhook call from prometheus alertmanager
	//
	//     Consumes:
	//     - application/json
	//
	//     Produces:
	//     - application/json
	///
	//     Responses:
	//       default: genericError
	//       200: someResponse
	//       422: validationError
	s.Router.HandleFunc("/webhook", s.HandleWithAuthorization(s.HandlePrometheusRequest(), *pluginsArray...)).Methods("POST")

	// swagger:route GET /webhook handleUsage
	//
	// Generates usage information for human beings
	//
	//     Consumes:
	//     - text/html
	//
	//     Produces:
	//     - text/html
	//
	//
	//     Responses:
	//       default: genericError
	//       200: someResponse
	//       422: validationError
	s.Router.HandleFunc("/webhook", s.handleUsage()).Methods("GET")
}
