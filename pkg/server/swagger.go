package server


var swaggerSpec = `{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "https"
  ],
  "swagger": "2.0",
  "info": {
    "description": "You have applications, applications have problems. You have Prometheus to help spot those problems.\n\nNow it's time to call TheWolf to solve them.\n\nThe Wolf is a flexible, plugin based problem resolver. It accepts notifications\n(currently only from Prometheus Alertmanager's Webhook Reciever) and triggers\na configured action based on the alert.\n\nActions are performed by plugins and can be templetized based on labels\nparsed from the alert.\n\nAccess to TheWolf is controlled by authorization plugins which can be chained\ntogether for more robust support.\n\nIn the future TheWolf will support additional plugin types. See the README for more information.",
    "title": "go:generate swagger generate spec\nPackage TheWolf",
    "termsOfService": "there are no TOS at this moment, use at your own risk we take no responsibility",
    "contact": {
      "name": "Aaron (TheSporkboy) Forster",
      "url": "https://bitbucket.org/theuberlab/thewolf",
      "email": "codemonkey@theuberlab.com"
    },
    "version": "0.1.1"
  },
  "basePath": "/",
  "paths": {
    "/": {
      "get": {
        "description": "Returns some directions for humans",
        "consumes": [
          "text/html"
        ],
        "produces": [
          "text/html"
        ],
        "operationId": "usage",
        "responses": {
          "200": {
            "$ref": "#/responses/someResponse"
          }
        }
      }
    },
    "/metrics": {
      "get": {
        "description": "This will show all available pets by default.\nYou can get the pets that are out of stock",
        "consumes": [
          "text/html",
          "application/x-protobuf"
        ],
        "produces": [
          "text/html",
          "application/x-protobuf"
        ],
        "summary": "Returns application metrics in prometheus format",
        "operationId": "prometheusmetrics",
        "responses": {
          "200": {
            "$ref": "#/responses/someResponse"
          }
        }
      }
    },
    "/webhook": {
      "get": {
        "description": "Generates usage information for human beings",
        "consumes": [
          "text/html"
        ],
        "produces": [
          "text/html"
        ],
        "operationId": "handleUsage",
        "responses": {
          "200": {
            "$ref": "#/responses/someResponse"
          },
          "422": {
            "$ref": "#/responses/validationError"
          },
          "default": {
            "$ref": "#/responses/genericError"
          }
        }
      },
      "post": {
        "description": "Accept webhook call from prometheus alertmanager",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "operationId": "HandleWithAuthorization",
        "parameters": [
          {
            "example": "\"webhook\"",
            "x-go-name": "Receiver",
            "name": "receiver",
            "in": "body",
            "schema": {
              "type": "string"
            }
          },
          {
            "x-go-name": "Status",
            "name": "status",
            "in": "body",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "x-go-name": "Alerts",
            "name": "alerts",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Alerts"
            }
          },
          {
            "x-go-name": "GroupLabels",
            "name": "groupLabels",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/KV"
            }
          },
          {
            "x-go-name": "CommonLabels",
            "name": "commonLabels",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/KV"
            }
          },
          {
            "x-go-name": "CommonAnnotations",
            "name": "commonAnnotations",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/KV"
            }
          },
          {
            "x-go-name": "ExternalURL",
            "name": "externalURL",
            "in": "body",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/someResponse"
          },
          "422": {
            "$ref": "#/responses/validationError"
          },
          "default": {
            "$ref": "#/responses/genericError"
          }
        }
      }
    }
  },
  "definitions": {
    "Alert": {
      "type": "object",
      "title": "Alert holds one alert for notification templates.",
      "required": [
        "status",
        "labels",
        "annotations",
        "startsAt",
        "endsAt"
      ],
      "properties": {
        "annotations": {
          "$ref": "#/definitions/KV"
        },
        "endsAt": {
          "type": "string",
          "format": "date-time",
          "x-go-name": "EndsAt",
          "example": "0001-01-01T00:00:00Z"
        },
        "generatorURL": {
          "type": "string",
          "x-go-name": "GeneratorURL",
          "example": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\\u0026g0.tab=1"
        },
        "labels": {
          "$ref": "#/definitions/KV"
        },
        "startsAt": {
          "type": "string",
          "format": "date-time",
          "x-go-name": "StartsAt",
          "example": "2018-08-03T09:52:26.739266876+02:00"
        },
        "status": {
          "description": "the id for this user",
          "type": "string",
          "x-go-name": "Status",
          "example": "\"firing\""
        }
      },
      "x-go-package": "bitbucket.org/theuberlab/thewolf/pkg/model"
    },
    "Alerts": {
      "type": "array",
      "title": "Alerts is a list of Alert objects.",
      "items": {
        "$ref": "#/definitions/Alert"
      },
      "x-go-package": "bitbucket.org/theuberlab/thewolf/pkg/model"
    },
    "KV": {
      "type": "object",
      "title": "KV is a set of key/value string pairs.",
      "additionalProperties": {
        "type": "string"
      },
      "x-go-package": "bitbucket.org/theuberlab/thewolf/pkg/model"
    }
  },
  "securityDefinitions": {
    "MutualTLS": {
      "type": "MutualTLS",
      "scopes": {
        "webhook": "webhook"
      }
    }
  },
  "security": [
    {
      "MutualTLS": []
    }
  ],
  "x-meta-array": [
    "value1",
    "value2"
  ],
  "x-meta-array-obj": [
    {
      "name": "obj",
      "value": "field"
    }
  ],
  "x-meta-value": "value"
}`