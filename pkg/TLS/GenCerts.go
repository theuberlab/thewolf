package TLS

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"log"
	"math/big"
	mathrand "math/rand"
	"os"
	"time"

	"bitbucket.org/theuberlab/lug"
)

func createKeyID() []byte {

	keyID := make([]byte, 10, 10)

	for i := 0; i < 10; i++ {
		keyID[i] = byte(mathrand.Intn(10))
		//keyID = append(keyID, byte(mathrand.Intn(10)))
	}

	return keyID
}

// Generates a certificate and key pair. If parentCert is nil the certificate will be self signed.
func GenKeyCertPair(certName string, parentCert *x509.Certificate, parentKey *rsa.PrivateKey) (*rsa.PrivateKey, []byte) {
	var signingCert *x509.Certificate
	var signingKey *rsa.PrivateKey

	// Prepare certificate
	cert := &x509.Certificate{
		SerialNumber:			big.NewInt(1658),
		Subject:	pkix.Name{
			Organization:		[]string{"ORGANIZATION_NAME"},
			Country:			[]string{"COUNTRY_CODE"},
			Province:			[]string{"PROVINCE"},
			Locality:			[]string{"CITY"},
			StreetAddress:		[]string{"ADDRESS"},
			PostalCode:			[]string{"POSTAL_CODE"},
			CommonName:			certName + ".localdomain",
		},
		NotBefore:				time.Now(),
		NotAfter:				time.Now().AddDate(10, 0, 0),
		SubjectKeyId:			createKeyID(),
		ExtKeyUsage:			[]x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		//KeyUsage:				x509.KeyUsageDigitalSignature,
		DNSNames:				[]string{certName + ".localdomain", certName},

		BasicConstraintsValid:	true,
	}

	mathrand.Intn(10)

	priv, _ := rsa.GenerateKey(rand.Reader, 2048)
	pub := &priv.PublicKey

	if parentCert != nil {
		log.Println("Generating signed certificate")
		lug.Debug("Message", "Generating signed certificate")
		signingCert = parentCert
		signingKey = parentKey
		cert.KeyUsage = x509.KeyUsageDigitalSignature
	} else {
		log.Println("Generating self-signed certificate")
		lug.Debug("Message", "Generating self-signed certificate")
		signingCert = cert
		signingKey = priv
		cert.KeyUsage = x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign
		cert.IsCA = true
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, cert, signingCert, pub, signingKey)
	if err != nil {
		lug.Error("Message", "Cert creation failed", "Error", err)
		os.Exit(1)
	}

	pemBytes := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})

	return priv, pemBytes
}

// Write a key/cert pair of files.
func WriteFiles(key *rsa.PrivateKey, cert []byte, name string)  {

	certFileName := name + ".crt"
	keyFileName := name + ".key"

	// Cert
	certOut, err := os.Create(certFileName)
	if err != nil {
		lug.Error("Message", "Error creating certificate file", "Error", err)
		os.Exit(1)
	}

	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: cert})
	certOut.Close()
	if err != nil {
		lug.Error("Message", "Error writing certificate", "Error", err)
		os.Exit(1)
	}

	lug.Info("Message", "Certificate written", "Filename", certFileName)


	// Private key
	keyOut, err := os.OpenFile(keyFileName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		lug.Error("Message", "Error creating key file", "Error", err)
		os.Exit(1)
	}

	pem.Encode(keyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(key)})
	keyOut.Close()
	if err != nil {
		lug.Error("Message", "Error writing key", "Error", err)
		os.Exit(1)
	}

	lug.Info("Message", "Certificate written", "Filename", keyFileName)
}