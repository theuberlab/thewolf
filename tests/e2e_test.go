package vegvisir_test

import (
	"bitbucket.org/theuberlab/lug"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	_ "bitbucket.org/theuberlab/thewolf/tests/e2eTests"
)

func init() {
	lug.InitLoggerFromYaml("lugTestConfig.yml")
}

func TestMajordomo(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "The Wolf E2e Test Suite")
}
