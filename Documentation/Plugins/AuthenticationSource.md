# Authentication Source PLugins

Authentication Source plugins are not yet implemented. 

They will provide a mechanism for TheWolf to fetch the credentials 
required to execute an action.

For example an SSH keystore plugin could allow TheWolf to fetch 
an unlocked SSH key which is stored in the SSH keychain of the host.

A token file plugin might fetch an authorization token from a configuration file.

A mutual TLS plugin could load a TLS client certificate, key and certificate
authority store, allowing an action plugin to use the certificate key pair 
to provide it's own credentials and the authority store to validate the
certificate provided by the server.