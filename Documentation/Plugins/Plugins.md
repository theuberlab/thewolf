# Plugins

In order to be flexible, secure and compact most of the significant 
functionality of TheWolf will be implemented through plugins.

These are implemented using [golang's native plugin architecture](https://golang.org/pkg/plugin/)
A fairly simple tutorial on writing golang plugins can be found 
[over here on medium](https://medium.com/learning-the-go-programming-language/writing-modular-go-programs-with-plugins-ec46381ee1a9)

# Plugin Types
The current design consists of a small number of plugins types. This list
is not solidified so not only may the names change but some may be 
abandoned, added or combined.

A set of "core" plugins is provided for the most common use cases. 
These plugins as well as their own development workflow can be found 
over in [TheWolf's Plugins repository](https://bitbucket.org/theuberlab/wolfplugins/)


# Accepted Plugin Types
The following plugin types have or will be implemented.

## [Action](Action.md)
Action plugins are the ones that actually DO things. Execute command 
line commands, ssh to servers, issue AWS API calls, etc.

## [Authorization](Authorization.md)
Authorization plugins will control the acceptance of TheWolf's job triggers.

## [Authentication Source](AuthenticationSource.md)
Authentication Source plugins will enable TheWolf Actions to fetch the 
credentials needed to interact with external systems. Examples could include
SSH keys, kubernetes secrets, certificate stores, fetching a username 
and password from stores such as Vault


# Proposed Plugin Types
Less well thought out plugin types which support functionality which
needs to be implemented. 

## Listeners/Parsers
In addition to the above there are a few other categories of plugins which 
may or may not see the light of day. The mostly likely of these is listeners
and/or parsers. The ideas are not fully fledged out.

In order to be flexible and be able to support accepting requests from systems
other than prometheus (like pagerduty, servicenow, etc) it would be nice
to implement plugins which perform at least the parsing.

Some of these systems may want to send requests in formats so different
from prometheus that we will need to create a separate listener. Alternatively 
some of these systems may send their requests using a protocol other than HTTPS.

Oh the other hand I do actually wish to avoid over architecting so 
at this point it would probably be best to avoid planning for things 
like SMTP support until TheWolf becomes significantly more mature.

### Listener
Listener plugins provide mechanisms for triggering Actions or series
of actions from an external source. This could include listening for an 
API request in various formats, triggering based on a timer, accepting an email.

### Parser
Parsers are the plugins which will take the body of a request in
whichever format and convert it to an internal Job object. They should
also validate the request though an argument could be made for that to be separate.



# Plugin Configuration
Since we don't know what the future holds or what new plugins will
be written to do and we don't want to update TheWolf's core code every
time new functionality is introduced (which is the main reason
for supporting plugins in the first place.) Plugins need to be able to 
provide their own configuration syntax. The 
[WolfPlugin interface](plugins/plugin.go) requires methods be 
implemented to perform such validation.

There are actually two components to this. 

# A Global Plugin Config
A plugin may wish to support some settings which apply to all instances 
of it's use.


# Action Instance Config
An action config section currently defines a few standard fields such as
the alert name, plugin name and comments and then a "Config" value which 
is simply passed to the plugin.