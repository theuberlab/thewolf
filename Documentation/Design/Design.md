# TheWolf Design


# Concepts
The thought process behind TheWolf


## Listeners (Listener Plugins?)
Listeners are the component which listen for an inbound request. There is currently only one listener The Prometheus Webhook listener.
We will want to turn this into a plugin based architecture so that we can listen for events from other systems.

## Actions
An action is the combination of an Alert name, an action script to execute and specific action settings such as concurrency. 
In the near future this will also be where you specify the authnz source and settings to fetch credentials neccessary for
calling your action script as well as a rollback script (probably just a call to a different Action?) In the far future
this will likely become some definition of a workflow.

## Action Plugins
Actions are the components that ultimately do something to resolve your problem. 
There is currently one action The External Command Action.

## Authorization Plugins
Authorization plugins will define how TheWolf determines if a caller is allowed to trigger a configured Action

## Authentication Source Plugins
Authentication source plugins will define how credential sources are accessed so that an Action can fetch credentials if needed.

# Really Undefined concepts

## Parsers
Something needs to parse the input for fields. This could be the Action itself or another type of plugin, a parser. 
Then Actions could provide their own or share a common one. Currently this is performed by the built in prometheus listener.

## Success/Failure Indicators (REALLY needs a name)
Determining success fail should be another separate component, rather than just relying on true false from the executed command. 
This would allow us to specify a more complex combination of timeout and status codes. 
I.E. Need to get three responses, need this response in this amount of time but if it takes 
longer need this other response. Or timeout after this long but if it returns in under this long consider it a failure.

# Configuration

At startup TheWolf will load it's configuration, the default location is currently $cwd/wolf.conf but this can be overridden with -c <path>

The configuration currently consists of an Actions element which is an array of alerts we expect to recieve and the command to execute if we recieve them.
It does not currently support different actions based on alert levels or do much with the pared api submission other than pull out the command name.

The configuration found in tests/e2eTests/thewolfconfig.yml is valid for the 3 existing plugins.

You can test the above config with the following curl command:

```curl -kv -H "Content-Type: application/json" -X POST https://localhost:8443/webhook --data '{"receiver": "webhook","status": "firing","alerts": [{"status": "firing","labels": {"team": "platform","severity": "Critical","alertname": "NodeNotReadyForTooLongJustOne"},"annotations": {"description": "Node ip-172-27-187-172.us-west-2.compute.internal has been in a NOT READY state for more than 9m, this should be investigated.","summary": "Node ip-172-27-187-172.us-west-2.compute.internal has been in a NOT READY state for more than 9m, thats generally not awesome."},"startsAt": "2018-08-03T09:52:26.739266876+02:00","endsAt": "0001-01-01T00:00:00Z","generatorURL": "http://simon-laptop:9090/graph?g0.expr=go_memstats_alloc_bytes+%3E+0\u0026g0.tab=1"}]}```

# TLS Configuration
TheWolf is HTTPS only. Upon startup it will look for a certificate and key pair in the environment variables THE_WOLF_CERTIFICATE and THE_WOLF_PRIVATE_KEY respectively.
If they are not found it will generate in memory it's own key and self signed certificate.

# Project Goals
This project currently has 3 primary goals

## An HTTPS endpoint which understands how to parse Prometheus webhook posts - This goal is complete!

## The ability to authorize said posts - This goal is complete!

## The ability to pass values from the post to arbitrary third party tools (scripts, binaries, etc running on the host.) - This goal is technically complete but I won't call it complete until we can authorize with mutualTLS

# AuthNZ

## Web hook AuthNZ
Taking action against your system is not something you want to expose to the world on port 80.
The Wolf will want to validate your requests.
Probably mutual TLS will be the simplest to implement.

# Actions
For each alert there will be a configurable action

## Action Concurrency

Different alerts will require different concurrency handling their solutions may need to:

 * Allow concurrent execution.
 * Block concurrent execution for a configurable amount of time.
 * Queue each request and execute them serially.

# The Future
Things it might be cool to include in the future.

## Down stream AutNZ
Depending upon what you need to clean up and how you need to clean it
The Wolf may need some credentials.

We should support multiple sources and decryption schemes.

### Credential Sources
The Wolf should probably support these sources (subject to change.)

 * Environment variables. These could be fed by something like kubernetes secrets or a deploy pipeline decoding and setting them.
 * (hopefully encrypted) files in cloud buckets like s3
 * Vault?
 * ssh keychain?
 * Other?

### Encryption
The Wolf should probably support these sources (subject to change.)

 * TLS
 * PGP

# Metrics
The Wolf will need to expose prometheus metrics for configured alerts.

 * Number of requests for the configured action
 * Number of successful executions
 * Number of failed executions Yes we want all 3 because concurrency settings could muddle the waters.
 * If queued que depth
 * Execution times (various buckets of summaries?)

# Datastore
For now everything will simply live in memory.

If we want to support queuing with multiple instances we'll need some form of cluster.
A shared datastore like etcd would be a good way to handle that.

# Clustering
Right now there isn't any. However TheWolf keeps no state information so it wouldn't be difficult to simply run multiple instances of TheWolf and stripe your requests across them.
WARNING: Once TheWolf supports concurrency settings running multiple instances will break that functionality.

# Plugins
Golang has a pretty nifty new plugin feature. Let's support that.

1. We might, in the future, want to accept requests from something other than prometheus webhooks.
1. We will definately at some point want to do something other than simply use gos os.exec.
1. I gaurentee I do not want to write all the potential integrations with Auth and credential sources.

# Workflow
Early versions of TheWolf will simply be a dumb reciever which executes a single command and returns it's results. 
However there is a reason that very few organizations employ automatic resolution of outages. 
It's VERY fragile.

For this to become more globally useful it would be good to integrate some form of workflow.
This could be built in or allow for integration with third parties.

But we would need to be able to answer these questions:

 * What do we do if the command didn't work?
 * What if it times out?
 * what if it's a sequence of commands?
 * How do you confirm that each one succeeded if return code is insufficient?
 * What do you do in case of partial suceess?





Other Unsorted Notes:


Since I want to make it modular anyway integration with an external workflow engine shouldn’t be too hard and doesn’t necessarily have to start soon. 

I’ve been looking at floe because it’s written in go but it’s api based so that doesn’t really matter anyway. 

https://github.com/floeit/floe/blob/master/cmd/floe/main.go

What is the benefit of the wolf over floe which can execute tasks?

TheWolf will already grok Prometheus requests. 
TheWolf will have a native understanding of rollback commands. Each resolution test can have one or more backout steps. Which might include a whole separate workflow. 
TheWolf will be able to generate Prometheus silences. 

TheWolf will produce a library of action plugins, Ssh command Action, kubernetes integration, AWS integration, External Command Action, template based api poster(unlike Prometheus) and more. 

Perhaps for a given configuration TheWolf can create a template for the workflow engine or configure it directly?

Securit, Security, security. 
TheWolf will have a built in(plugin based) credential store integration and a flexible policy based authorization engine. 

Additionally. 
When interacting with a workflow system TheWolf should inject a one time token that is used for each step of the workflow which will return to wolf so that someone can it take advantage of the lower security of the WF system to make two of compromise something. 

This will also mean integrated loggers for things like spunk and some day integration with external security hardware. 

Where possible have automatic roll back steps auto generated. A “delete file” resolver could take a backup pattern or backup directory, copy the file, delete the Orig, if the rollback step is called on this phase we copy it back. 

TheWolf should also understand Prometheus metrics. We could do fancy things like if an alert is triggered start consuming it’s graph. If the alert is connections too high it should start watching the graph and be configured for what to do in that situation. 



Security
Some things to keep in mind and/or implement to improve security. 

Don’t expose information in the URL. I’m already avoiding it but not as purposefully just as an accident. Keep it forefront. 

Request time stamps. If I end up doing multiple request actions consider evaluating timestamps combined with with tokens to help mitigate man in the middle attacks. 

Http basic can be used to implement one time tokens by just stuffing them in the username field. 

Many things talk about oath as better but do they just mean when users are involved.  


TheWolf (or maybe just TheUberlab will need an a security announce mechanism. I wonder what the free tier of slack gets you. 

Peep this article on mutualTLs. He also references some go code to generate both server and client certs.
http://www.levigross.com/2015/11/21/mutual-tls-authentication-in-go/



So, as suspected mutualTLS will require modifying the TLS options handed to the http library when I start listening. 

There’s probably some hoopty I can go through to make it work passing functions around but for expediency lets just implement that in the main server code. Later we can worry about splitting it out. 

I can probably still build an authorized function which parses the certificate for appropriate subject or whatever. 

TheWolf will support a client CA (or eventually list of cas it will combine together) and a flag for requireAndVerify. I may or may not want to add ask as an option there. (Though it might be a sketchy way to implement the plugin.) then the authorization plugin could verify various components of the cert. subject, issuer, etc. so the plugin will only be useful if the functionality is enabled. Maybe there’s a way the plugin can check and complain if it isn’t. 

Maybe plugins can provide a TLS config. And at startup we loop through them and if one provides that function we call it otherwise we go with the built in config. Then plugins could fight over TLS settings. 

Maybe authorizationugins can have a requiresTLSComfigOptions() method that returns required options and they can refuse to start if they aren’t enabled?
