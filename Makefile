PROJECT_ROOT := $(shell git rev-parse --show-toplevel)
include $(PROJECT_ROOT)/Makefile.variables
# Vars
# This is the home of dynamically defined variables, variable paths etc.
# All other vars should live in Makefile.variables.
PKGS := $(shell go list ./... | grep -v /vendor)
BIN_DIR := $(GOPATH)/bin
GOMETALINTER := $(BIN_DIR)/gometalinter
VERSION ?= $(MAIN_VERSION)
BUILD ?= `date -u +build-%Y%m%d.%H%M%S` # Not actually using this yet but I want to.
os = $(word 1, $@)
currOS := $(shell uname | tr '[:upper:]' '[:lower:]')
BUILDDIR := $(PROJECT_ROOT)/build
# Temporarily using this until we are publishing automatially. #TODO: This should _probably_ be under BUILDDIR if we keep it.
OUTPUTDIR := $(PROJECT_ROOT)/release

# Makeshift make "help" command.
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: targets
targets: ## Prints targets and their documentation. Currently does not work with dynatmic targets, I.E. ($(OUTPUTDIR)):
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

###############################################################################################
# Bazel Targets
# These are the temporary targets for bazel until we get out from under it
###############################################################################################


.PHONY: thewolf
thewolf: $(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(currOS)-amd64 ## Builds TheWolf with bazel

$(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(currOS)-amd64: $(OUTPUTDIR) ## Creates TheWolf's server binary
	bazel build thewolf
	cp ./bazel-bin/darwin_amd64_stripped/thewolf $(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(currOS)-amd64

.PHONY: plugins
plugins: $(OUTPUTDIR)/plugins/$(currOS) WolfPlugins $(OUTPUTDIR)/plugins/$(currOS)/TEXTResponder.so $(OUTPUTDIR)/plugins/$(currOS)/ExternalCommandExecutor.so $(OUTPUTDIR)/plugins/$(currOS)/HTTPBasic.so ## Common target for all plugins

#TODO: Make these based on the $(PLUGINS) list or dynamically generate a list based on the actual source code which is available.

$(OUTPUTDIR)/plugins/$(currOS)/TEXTResponder.so:
	bazel build //WolfPlugins/Action/TEXTResponder:TEXTResponder.so
	cp bazel-bin/WolfPlugins/Action/TEXTResponder/darwin_amd64_stripped_plugin/TEXTResponder.so.dylib $(OUTPUTDIR)/plugins/$(currOS)/TEXTResponder.so

$(OUTPUTDIR)/plugins/$(currOS)/ExternalCommandExecutor.so:
	bazel build //WolfPlugins/Action/ExternalCommandExecutor:ExternalCommandExecutor.so
	cp bazel-bin/WolfPlugins/Action/ExternalCommandExecutor/darwin_amd64_stripped_plugin/ExternalCommandExecutor.so.dylib $(OUTPUTDIR)/plugins/$(currOS)/ExternalCommandExecutor.so

$(OUTPUTDIR)/plugins/$(currOS)/HTTPBasic.so:
	bazel build //WolfPlugins/Authorization/HTTPBasic:HTTPBasic.so
	cp bazel-bin/WolfPlugins/Authorization/HTTPBasic/darwin_amd64_stripped_plugin/HTTPBasic.so.dylib $(OUTPUTDIR)/plugins/$(currOS)/HTTPBasic.so

.PHONY: clean
clean: ## Removes both build and output directories.
#	-echo y | rm -r $(BUILDDIR) # This is currently not being created.
	-echo y | rm -r $(OUTPUTDIR)
	-echo y | rm WolfPlugins

cleanwolfplugins: ## Removes the built plugin binaries.
#	echo "Deleting the following files:"
#	ls $(OUTPUTDIR)/plugins/$(currOS)/
	-echo y | rm $(OUTPUTDIR)/plugins/$(currOS)/*

../wolfplugins: ## Does a go get of the plugins repo.
	-go get -u bitbucket.org/theuberlab/wolfplugins

WolfPlugins: ../wolfplugins ## Creates the symlink to the plugins directory
	ln -s $(PROJECT_ROOT)/../wolfplugins $(PROJECT_ROOT)/WolfPlugins

$(OUTPUTDIR): ## Creates the base output directory.
	mkdir $@

$(OUTPUTDIR)/plugins: $(OUTPUTDIR) ## Creates the base plugins directory
	mkdir $@

$(OUTPUTDIR)/plugins/$(currOS): $(OUTPUTDIR)/plugins/ ## Creates the plugins output directory for the current OS.
	mkdir $@

.PHONY: demo
demo: thewolf plugins ## Serve TheWolf with the demo settings
	$(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(currOS)-amd64 -c ./tests/e2eTests/thewolfconfig.yml -P $(OUTPUTDIR)/plugins/$(currOS)


#TODO: Add a make target for generating the certs

###############################################################################################
# Non-Bazel Targets
# These are the targets I want to actually use.
###############################################################################################

## This works to only re-compile the binary if the file main.go when the target is a constant.
## I want to update some of the targets below to work this way so I will have to do a little dance.
## Perhaps version doesn't include the second, only the day or a hash or something.
## Then I will have to make the target be the file name instead of just the platform.
#$(currOS): pkg/main.go
#	@echo "Made for $(currOS)"
##	GOOS=darwin GOARCH=amd64 go build -ldflags "-X main.Version=$(VERSION)"


##TODO: Update these two targets to work in a pipeline. See ticket
#.PHONY: deploy
#deploy:
#	@echo "Deploying"
#	if [[ "$(namespace)" == "" ]]; then          \
#	    read -ep "Target namespace: " namespace; \
#	fi;                                          \
#	if [[ "$${namespace}" != "" ]]; then         \
#	    kubectl -n $${namespace} apply           \
#	        --filename k8s-resources             \
#	        --record;                            \
#    fi
#
#.PHONY: teardown
#teardown:
#	@echo "Tearing Down"
#	if [[ "$(namespace)" == "" ]]; then                   \
#	    read -ep "Target namespace: " namespace;          \
#	fi;                                                   \
#	if [[ "$${namespace}" != "" ]]; then                  \
#	    kubectl -n $${namespace} delete -f k8s-resources; \
#	fi

#
##TODO: Make this work with the new binary file name and directory and add some requirements for bits that actually makes them.
#serve: $(currOS) ## Runs the built binary
#	$(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(currOS)-amd64
#
#$(BUILDDIR): ## Create the temporary build directory
#	mkdir $(BUILDDIR)
#
#$(OUTPUTDIR): ## Create the output directory
#	mkdir $(OUTPUTDIR)
#
#
#
##.PHONY: $(PLATFORMS)
##TODO: Make this work. Right now we're not actually doing the .pkg thing but we should so that we get to use the root of the repo for additional things without polluting it so much.
#$(PLATFORMS): $(BUILDDIR) ## Build TheWolf for both platforms.
#	@echo "Making TheWolf for $(os)"
#	GOOS=$(os) GOARCH=amd64 go build -ldflags "-X main.Version=$(VERSION)" -o $(OUTPUTDIR)/$(BINARY)-$(VERSION)-$(os)-amd64 .
#	@echo "Making plugins for $(os)"
#	@for pluginSource in $(PLUGINS); do \
#		pluginOutLong=$$(basename $$pluginSource); \
#		pluginOut=$${pluginOutLong%%.*}; \
#		echo "Making plugin $$pluginOut"; \
#		echo "Issuing command GOOS=$(os) GOARCH=amd64 go build -buildmode=plugin -o $(OUTPUTDIR)/$(os)/$${pluginOut}-$(VERSION)-$(os)-amd64.so $$pluginSource"; \
#		CGO_ENABLED=0 GOOS=$(os) GOARCH=amd64 go build -buildmode=plugin -o $(OUTPUTDIR)/$(os)/$${pluginOut}-$(VERSION)-$(os)-amd64.so $$pluginSource; \
#	done
#
#.PHONY: release
#release: linux darwin ## Build the binaries


###############################################################################################
# Test Targets
###############################################################################################

.PHONY: lint
lint: $(GOMETALINTER) ## Run the linter
	@echo "Running linter"
	gometalinter $(PKGS)

.PHONY: test
test: lint ## Run the unit tests
	@echo "running go test"
	go test $(PKGS)

$(GOMETALINTER): ## Downloads and builds gometalinter for linting
	go get -u github.com/alecthomas/gometalinter
	gometalinter --install &> /dev/null

#TODO: Using the demo target is still a bit hacky. We actually ant to spin or scale up a k8s cluster and deploy
# some containers (not just for TheWolf.) _probably_ don't want to do that here in the makefile but in our pipeline file
# so that the makefile can remain more generic.

.PHONY: e2e
e2e: demo ## Run the e2e test suite.
	@echo "Running end to end test suite."
	go test -v ./tests/e2e_test.go
	pkill thewolf
