//go:generate swagger generate spec
// Package TheWolf
//
// You have applications, applications have problems. You have Prometheus to help spot those problems.
//
// Now it's time to call TheWolf to solve them.
//
// The Wolf is a flexible, plugin based problem resolver. It accepts notifications
// (currently only from Prometheus Alertmanager's Webhook Reciever) and triggers
// a configured action based on the alert.
//
// Actions are performed by plugins and can be templetized based on labels
// parsed from the alert.
//
// Access to TheWolf is controlled by authorization plugins which can be chained
// together for more robust support.
//
// In the future TheWolf will support additional plugin types. See the README for more information.
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: https
//     BasePath: /
//     Version: 0.0.6
//     Contact: John Doe<john.doe@example.com> https://bitbucket.org/theuberlab/thewolf
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - MutualTLS:
//
//     SecurityDefinitions:
//     MutualTLS:
//       type: MutualTLS
//       scopes:
//         webhook: webhook
//
//     Extensions:
//       x-meta-value: value
//       x-meta-array:
//         - value1
//         - value2
//       x-meta-array-obj:
//         - name: obj
//           value: field
//
// swagger:meta
package main

//     ExternalDocs:
//       Description: Find out more about TheWolf
//       url: 'https://bitbucket.org/theuberlab/thewolf'
//

// TODO: Figure out what mutualTLS should look like.
// TODO: I don't really know what the extensions are for.

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"net/http"
	"os"
	"time"

	"gopkg.in/alecthomas/kingpin.v2"

	"bitbucket.org/theuberlab/lug"
	"bitbucket.org/theuberlab/thewolf/config"
	"bitbucket.org/theuberlab/thewolf/pkg"
	"bitbucket.org/theuberlab/thewolf/pkg/server"
	"bitbucket.org/theuberlab/thewolf/plugins"
)

// General variables
var (
	// Byte arrays to store the private key and certificate we will use to serve TLS
	keyPEM			[]byte
	certPEM			[]byte
	caPEM			[]byte
	configFile		string
	pluginsDir		string
	logLevel		string
	logFormat		string
	logUTC			bool
	apiport			int
	appVersion		string
	ctx				context.Context
	metricsArray	*VerifyMetrics
	configuration	config.Configuration
	pluginManager	plugins.PluginManager
)

// Variables that will be used as handles for specific activities.
var (
	// Creates the top level context for all commands flags and arguments
	app      			= kingpin.New("podstartupwatcher", "Connects to the kubernetes API and watches pod events while it spins up and destroys pods.")
	versionInfo			*VersionInfo
)

// Initialize some components that need to be available early on.
func init() {
	versionInfo = &VersionInfo{} // Doing this weird thing due to some legacy reasons. Should probably drop it.
	appVersion = versionInfo.GetVersion()

	// Setup Kingpin flags
	// Set the application version number
	app.Version(appVersion)
	// Allow -h as well as --help
	app.HelpFlag.Short('h')

	app.Flag("apiport", "The port the API server should listen on. Can be set via THE_WOLF_API_PORT.").Short('A').Default("8443").Envar("THE_WOLF_API_PORT").IntVar(&apiport)

	app.Flag("config", "The Wolf's configuration file.").Short('c').Default("wolf.conf").Envar("THE_WOLF_CONFIG_FILE").ExistingFileVar(&configFile)

	app.Flag("plugins", "Location to load TheWolf's plugins from.").Short('P').Default("./plugins").Envar("THE_WOLF_PLUGINS_DIR").ExistingDirVar(&pluginsDir)

	// Logging flags
	app.Flag("log-utc", "Timestamp lug.Log messages in UTC instead of local time. Can be set via THE_WOLF_LOG_UTC").Default("false").Envar("THE_WOLF_LOG_UTC").BoolVar(&logUTC)
	app.Flag("logLevel", "The level of lug.Logging to use. Can be set via THE_WOLF_LOG_LEVEL").Short('l').Default("Error").Envar("THE_WOLF_LOG_LEVEL").EnumVar(&logLevel, "None",
		"Error",
		"Warn",
		"Info",
		"Debug",
		"All")
	app.Flag("logformat", "What output format to use. Can be set via THE_WOLF_LOG_FMT").Short('f').Default("logfmt").Envar("THE_WOLF_LOG_FMT").EnumVar(&logFormat, "logfmt", "json")

	// Now parse the flags
	kingpin.MustParse(app.Parse(os.Args[1:]))

	lug.InitLoggerFromYaml("lugConfig.yml")

	initCert()
}

// Pulls in or creates the certificate and key.
func initCert() {
	//TODO: Turn these into command line arguments.
	keyPEMStr := os.Getenv("THE_WOLF_PRIVATE_KEY")
	certPEMStr := os.Getenv("THE_WOLF_CERTIFICATE")
	caPEMStr := os.Getenv("THE_WOLF_AUTHORITY_DATA")

	// If either of those are empty
	if keyPEMStr == "" || certPEMStr == "" {
		if keyPEMStr == "" {
			lug.Warn("Message", "Failed to find key")
		}
		if certPEMStr == "" {
			lug.Warn("Message", "Failed to find Certificate")
		}
		lug.Warn("Message", "Generating key and self-signed certificate")
		// Generate a default key and cert
		privKey, pubKey := pkg.GenKeyPair(2048)

		keyPEM = pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privKey)})
		certPEM = pkg.GenCert(privKey, pubKey)
	} else {
		lug.Warn("Message", "Certificate and key found")
		pemStr, err := base64.StdEncoding.DecodeString(keyPEMStr)
		if err != nil {
			lug.Error("Message", "Failed to decode key", "Error", err)
		}
		certStr, err := base64.StdEncoding.DecodeString(certPEMStr)
		if err != nil {
			lug.Error("Message", "Failed to decode cert", "Error", err)
		}
		keyPEM = []byte(pemStr)
		certPEM = []byte(certStr)
	}

	if caPEMStr != "" {
		lug.Info("Message", "Decoding CA Data")
		caStr, err := base64.StdEncoding.DecodeString(caPEMStr)
		if err != nil {
			lug.Error("Message", "Failed to decode authority data", "Error", err)
		}

		caPEM = []byte(caStr)
	}
}

func main() {
	// Initialize the prometheus counters and the /metrics path
	metricsArray = NewVerifyMetrics()
	metricsArray.RegisterAllMetrics()

	err := config.LoadConfig(configFile, &configuration)
	if err != nil {
		lug.Error("Message", "Error loading wolfConfig", "Error", err)
	}

	// configuration is still empty here.
	lug.Debug("Message", "Configuration", "Config", fmt.Sprintf("%v", configuration))

	pluginManager.InitPlugins(pluginsDir)

	wolfServer := server.GetWolfServer(&pluginManager, &configuration)

	wolfServer.MakeRoutes()
	//whook.InitWebhook(&configuration, pluginManager)

	// Configure TLS
	cer, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		lug.Error("Message", "Failed", "Error", err)
	}

	tlsConfig := &tls.Config{
		// Set the server certificate
		Certificates: []tls.Certificate{cer},
		//TODO: Do something like this and enforce a higher grade of security
		//CipherSuites: []uint16{tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384},
		// Require the above cipher suites
		//PreferServerCipherSuites: true,
		// Require TLS 1.2
		MinVersion: tls.VersionTLS12,
	}

	if caPEM != nil {
		clientCertPool := x509.NewCertPool()
		if ok := clientCertPool.AppendCertsFromPEM(caPEM); !ok {
			lug.Error("Message", "Unable to add certificate to CA certificate pool")
		}

		lug.Debug("Message", "CA Cert found")

		tlsConfig.ClientCAs = clientCertPool
		tlsConfig.ClientAuth = tls.RequireAndVerifyClientCert

		//tlsConfig = &tls.Config{
		//	// Set the server certificate
		//	Certificates: []tls.Certificate{cer},
		//	// Set the CA pool to use to validate certificates
		//	ClientCAs: clientCertPool,
		//	// Require client certificates which can be verified by the above pool
		//	ClientAuth: tls.RequireAndVerifyClientCert,
		//	//TODO: Do something like this and enforce a higher grade of security
		//	//CipherSuites: []uint16{tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384},
		//	// Require the above cipher suites
		//	//PreferServerCipherSuites: true,
		//	// Require TLS 1.2
		//	MinVersion: tls.VersionTLS12,
		//}

		tlsConfig.BuildNameToCertificate()

	}


	listener, err := tls.Listen("tcp", fmt.Sprintf(":%d", apiport), tlsConfig)
	if err != nil {
		lug.Error("Message", "Failed", "Error", err)
	}

	httpServer := http.Server{
		Handler:      wolfServer.GetRouter(),
		TLSConfig:    tlsConfig,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
	}

	//TODO: Do something smarter here (will have to understand go's native context a bit more for that.)
	var cancel context.CancelFunc
	ctx, cancel = context.WithCancel(context.Background())
	defer cancel() // cancel when we are finished

	//http.ServeTLS(listener, router, getMux(versionInfo))
	lug.Info("Message", "Application startup", "Version", BaseVersion, "APIPort", apiport)

	//TODO: Make this thing threaded
	fmt.Printf("Failed with error %v", httpServer.Serve(listener))
}