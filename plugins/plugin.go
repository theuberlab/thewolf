package plugins

import (
	"strings"
	"time"

	"bitbucket.org/theuberlab/lug"
)

// All plugins must implement WolfPlugin
type WolfPlugin interface {
	GetPluginVersion() 					string // Returns a version in symantic format.
	GetPluginName() 					string // Returns the name of the plugin. The value of "Plugin" in an action config needs to match this value.
	GetPluginType() 					PluginType
	ValidatePluginConfig(interface{}) 	error	// This idea is still not well formed. Though there IS a reason for it to be Validate.. instead of Get... because this will only happen once at startup instead of many times for each and every configured action which references the plugin.
	// Maybe we should define a type which includes base options and then a separate config section. Or probably an interface which includes a set of expected attributes but can then also include more.
	//TODO: Figure out how to implement the below. I think there's a pattern in an article I've read. So we can hand it a metrics object and it will add all the metrics it will expose for prometheus style scraping.
	// RegisterMetrics(prometheusMetricsObject) // to add its own metrics to be scraped. We should publish a standard for minimum metrics and expected formats. I don’t _think_ we can enforce this with an interface but will try.
}


// The plugins configuration section is just a container for configs for each plugin type.
type PluginsConfig struct {
	Action	 			map[string]ActionPluginConfig 					`yaml:"Action,omitempty"`
	AuthSource	 		map[string]AuthenticationSourcePluginConfig		`yaml:"AuthenticationSource,omitempty"`
	Authorization 		map[string]AuthorizationPluginConfig			`yaml:"Authorization,omitempty"` 		// I guess in some world there is an authorization plugin which won't require any configuraton
	Listener 			map[string]ListenerPluginConfig					`yaml:"Listener,omitempty"`
	Parser 				map[string]ParserPluginConfig					`yaml:"Parser,omitempty"`
	Comments			string 											`yaml:"Comments,omitempty"`			// Allows us to support json config files without customers loosing comments.
}

//TODO: We should probably have an interface for plugin config types so that when finally figure out how the whole
// "It specifies it's own Config format" thing will work we can make sure to implement it across the board for all plugin types.

// Each plugin type needs to include a Config section which will be specified by the individual plugin
type ActionPluginConfig struct {
	Plugin 				string				`yaml:"Plugin"`						// The name of the plugin as it will be referenced in the config file. Must match what the plugin returns in response to GetPluginName()
	DefaultTimeout 		time.Duration 		`yaml:"DefaultTimeout,omitempty"` 	// How long before giving up. Overrides the global timeout for calls to this plugin.
	Comments			string 				`yaml:"Comments,omitempty"`			// Allows us to support json config files without customers loosing comments.
	Configuration		interface{}			`yaml:"Configuration"`
	// Then possibly later we can do something like this?
	//DefaultOnFailAction
	//DefaultOnSuccessAction
	//DefaultOnTimeoutAction
}

type AuthenticationSourcePluginConfig struct {
	Plugin 				string				`yaml:"Plugin"`						// The name of the plugin as it will be referenced in the config file. Must match what the plugin returns in response to GetPluginName()
	DefaultTimeout 		time.Duration 		`yaml:"DefaultTimeout,omitempty"` 	// How long before giving up. Overrides the global timeout for calls to this plugin.
	Comments			string 				`yaml:"Comments,omitempty"`			// Allows us to support json config files without customers loosing comments.
	Configuration		interface{}			`yaml:"Configuration"`
}

type AuthorizationPluginConfig struct {
	Plugin 				string				`yaml:"Plugin"`						// The name of the plugin as it will be referenced in the config file. Must match what the plugin returns in response to GetPluginName()
	DefaultTimeout 		time.Duration 		`yaml:"DefaultTimeout,omitempty"` 	// How long before giving up. Overrides the global timeout for calls to this plugin.
	Comments			string 				`yaml:"Comments,omitempty"`			// Allows us to support json config files without customers loosing comments.
	Configuration		interface{}			`yaml:"Configuration"`
	Order 				int 				`yaml:"Order"`						// Relatively certain I want to do this differently. Currently just a 0-whatever of ints and the plugins will be chained in that order.
}

type ListenerPluginConfig struct {
	Plugin 				string				`yaml:"Plugin"`						// The name of the plugin as it will be referenced in the config file. Must match what the plugin returns in response to GetPluginName()
	DefaultTimeout 		time.Duration 		`yaml:"DefaultTimeout,omitempty"` 	// How long before giving up. Overrides the global timeout for calls to this plugin.
	Comments			string 				`yaml:"Comments,omitempty"`			// Allows us to support json config files without customers loosing comments.
	Configuration		interface{}			`yaml:"Configuration"`
}

type ParserPluginConfig struct {
	Plugin 				string				`yaml:"Plugin"`						// The name of the plugin as it will be referenced in the config file. Must match what the plugin returns in response to GetPluginName()
	DefaultTimeout 		time.Duration 		`yaml:"DefaultTimeout,omitempty"` 	// How long before giving up. Overrides the global timeout for calls to this plugin.
	Comments			string 				`yaml:"Comments,omitempty"`			// Allows us to support json config files without customers loosing comments.
	Configuration		interface{}			`yaml:"Configuration"`
}


// Create the PluginType enum
type PluginType int

const (
	PLUGIN_TYPE_NONE			PluginType	= iota
	PLUGIN_TYPE_ACTION
	PLUGIN_TYPE_AUTHN_SOURCE
	PLUGIN_TYPE_AUTHORIZATION
	PLUGIN_TYPE_LISTENER
	PLUGIN_TYPE_PARSER
	PLUGIN_TYPE_ERROR						= 100 // If we get even close to this we're doing something wrong
)

// Gets the string representation of a PluginType
func (p *PluginType) String() string {
	names := [...]string{
		"PLUGIN_TYPE_NONE",
		"PLUGIN_TYPE_ACTION",
		"PLUGIN_TYPE_AUTHN_SOURCE",
		"PLUGIN_TYPE_AUTHORIZATION",
		"PLUGIN_TYPE_LISTENER",
		"PLUGIN_TYPE_PARSER",
		"PLUGIN_TYPE_ERROR",
	}

	if *p < PLUGIN_TYPE_NONE || *p > PLUGIN_TYPE_ERROR {
		return "NONE"
	}

	return names[*p]
}

//Turn a string into an enum
func PluginTypeFromString(pluginString string) PluginType {
	enums := make(map[string]PluginType)

	enums["PLUGIN_TYPE_ERROR"]			= PLUGIN_TYPE_ERROR
	enums["PLUGIN_TYPE_ACTION"]			= PLUGIN_TYPE_ACTION
	enums["PLUGIN_TYPE_LISTENER"]		= PLUGIN_TYPE_LISTENER
	enums["PLUGIN_TYPE_PARSER"]			= PLUGIN_TYPE_PARSER
	enums["PLUGIN_TYPE_AUTHORIZATION"]	= PLUGIN_TYPE_AUTHORIZATION
	enums["PLUGIN_TYPE_AUTHN_SOURCE"]	= PLUGIN_TYPE_AUTHN_SOURCE
	enums["PLUGIN_TYPE_NONE"]			= PLUGIN_TYPE_NONE

	lug.Trace("Message", "normalizing string", "String", pluginString)
	pluginString = strings.ToUpper(pluginString)

	lug.Trace("Message", "Normalized", "String", pluginString, "EnumValue", enums[pluginString])


	return enums[pluginString]
}
